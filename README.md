# Places

Places is a aggregate dataset of Points of Interest collected from first party
sources.

## License

Any database rights I may or may not have gained collecting this data is waved,
but all copyright is still owned by the first parties as this data comes from
their websites.

## Accuracy

Due to this being a collection of different datasets, the accuracy differs quite
a bit.
Most of the data is good enough, depending on your use case you may consider
merging this dataset with OpenStreetMap.
You'd probably want to take opening times, and contact details from this
dataset, but take locations from OpenStreetMap.

Originally this data was purely from the source websites, but I have been
getting more lenient on making corrections here to make the dataset more usable
and useful.
If you see any major issues please contact me.

## Contact

If you work at one of the upstream companies, please contact
[me](mailto:me@keepawayfromfire.co.uk).
I can probably help improve some issues with your website and/or data.

## New sources

Please check if an
[issue exists](https://gitlab.com/KAFF/places/-/issues?sort=created_date&state=all)
(open or closed), if not open one with a a link to the store finder and the
"Potential Source" label.


### Rules

I have some rules on which sites I collect data from that I use as a proxy for
consent.
If you are from an upstream company an want to explicitly consent, or ask me to
stop, please contact me.

**robots.txt**

I check for wildcards or `KAFF-Places`.
If I'm denied from the the required web pages or sitemap I will not pull data
from the website.

**Sitemap**

There must be a xml sitemap that includes all the relevant web pages.

**Structured Data**

The web pages for the POI must contain [Structured Data](https://schema.org/).
I can currently parse Microdata or JSON Linked Data.

## Updates

🤷

I should do this on a schedule, but I don't currently.
Please convince me to.

